package com.vhl.smscaster.braodcastReceivers

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.SmsManager
import com.google.zxing.client.result.SMSMMSResultParser
import com.vhl.smscaster.messaging.SmsListener

class MessageSentBR(val smsListener: SmsListener) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        when(resultCode) {
            Activity.RESULT_OK -> smsListener.updateMessageSent()
            SmsManager.RESULT_ERROR_GENERIC_FAILURE -> {
                smsListener.updateMessageFailed()
            }
            SmsManager.RESULT_ERROR_NO_SERVICE -> {
                smsListener.updateMessageFailed()
            }
            SmsManager.RESULT_ERROR_NULL_PDU -> {
                smsListener.updateMessageFailed()
            }
            SmsManager.RESULT_ERROR_RADIO_OFF -> {
                smsListener.updateMessageFailed()
            }

        }
    }

}