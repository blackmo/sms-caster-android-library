package com.vhl.smscaster.messaging.readers

import android.content.Context
import android.database.Cursor
import android.net.Uri
import com.vhl.sms.caster.model.MessageReceived
import com.vhl.sms.caster.model.MessageToSend
import com.vhl.smscaster.contacts.PhoneBook
import org.kodein.di.Kodein
import org.kodein.di.generic.instance
import org.kodein.di.newInstance

class MessageReader  (private val phoneBook: PhoneBook) {

   var drafts     = arrayListOf<MessageToSend>()
   var sent       = arrayListOf<MessageToSend>()
   var received   = arrayListOf<MessageReceived>()


   companion object {

      const val URI_INBOX        = "content://sms/inbox"
      const val URI_SENT         = "content://sms/sent"
      const val URI_DRAFT        = "content://sms/draft"

      private const val ID       = "_id"
      private const val ADDRESS  = "address"
      private const val DATE     = "date"
      private const val BODY     = "body"

      fun create(kodein: Kodein): MessageReader {
         val messageReader : MessageReader by kodein.instance()
         return messageReader
      }
   }

   fun getMessages(uriType: String, context: Context): List<Any>? {
      val uri  = Uri.parse(uriType)
      val cursor = context.contentResolver.query(uri, arrayOf(ID, ADDRESS, DATE, BODY), null, null, null)

      return cursor?.let {
         createMessageList(uriType, it).also {cursor.close() }
      }
   }

   private fun createMessageList(uri: String, cursor: Cursor): List<Any> {
      val listMessages =  mutableListOf<Any>()
      while (cursor.moveToNext()) {
        listMessages.add(getMessageType(uri, cursor))
      }
      return listMessages
   }

   private fun getMessageType(uri: String, cursor: Cursor) =
      when(uri) {
         URI_INBOX ->  MessageReceived(
            id       = cursor.getInt(0),
            body     = cursor.getString(3),
            date     = cursor.getString(2),
            sender   = phoneBook.getContactByPhoneNumber(cursor.getString(1))
         )

         URI_SENT -> MessageToSend(
            id          = cursor.getInt(0),
            body        = cursor.getString(3),
            date        = cursor.getString(2),
            recipient   = phoneBook.getContactByPhoneNumber(cursor.getString(1)),
            sent        = true
         )

         else -> MessageToSend (
            id          = cursor.getInt(0),
            body        = cursor.getString(3),
            date        = cursor.getString(2),
            recipient   = phoneBook.getContactByPhoneNumber(cursor.getString(1)),
            sent        = false
         )
      }
}
