package com.vhl.smscaster.messaging

import com.vhl.sms.caster.model.MessageToSend

interface Messenger {
    fun sendMessage(message: MessageToSend)
}
