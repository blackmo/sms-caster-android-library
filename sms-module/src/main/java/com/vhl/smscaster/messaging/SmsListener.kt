package com.vhl.smscaster.messaging

interface SmsListener {
    fun updateMessageSent()
    fun updateMessageFailed()
}