package com.vhl.smscaster.messaging

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.provider.Telephony
import android.telephony.SmsManager
import com.vhl.sms.caster.model.MessageToSend
import com.vhl.smscaster.braodcastReceivers.MessageSentBR

class SmsHandler(private val context: Context): Messenger, SmsListener {

    private val SENT = "SMS_SENT"
    private val DELIVERED = "SMS_DELIVERED "

    override fun updateMessageSent() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateMessageFailed() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun sendMessage(message: MessageToSend) {
        val intent = Intent(context, Telephony.Sms::class.java)
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
        val smsManager = SmsManager.getDefault()

        smsManager.sendTextMessage(message.getRecipeintNumber(), null,  message.body, pendingIntent, null)
        context.setBroadcastListener(this)
    }

    private fun Context.setBroadcastListener(smsListener: SmsListener) {
        val brSend = MessageSentBR(smsListener)
        this.registerReceiver(brSend, IntentFilter(SENT))
    }

}


