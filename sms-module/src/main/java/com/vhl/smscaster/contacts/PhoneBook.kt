package com.vhl.smscaster.contacts

import android.content.Context
import android.database.Cursor
import android.provider.ContactsContract
import com.vhl.sms.caster.model.Contact
import org.kodein.di.Kodein
import org.kodein.di.generic.instance

class PhoneBook {

    var phoneBookContacts = arrayListOf<Contact>()

    companion object {
           fun create(kodein: Kodein): PhoneBook {
               val phoneBook: PhoneBook by kodein.instance()
               return phoneBook
           }
    }

    fun getContacts(context: Context): List<Contact>? {
        val cursor =  context.contentResolver
            .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null)

        return cursor?.let {
            createContactList(it)
                .also { cursor.close() }
        }
    }

    private fun createContactList(cursor: Cursor): List<Contact> {
        val contacts = mutableListOf<Contact>()
        while(cursor.moveToNext()) {
            contacts.add(
                Contact(
                    name = cursor.getString(cursor.getColumnIndex(
                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
                    )),
                    number = cursor.getString(cursor.getColumnIndex(
                        ContactsContract.CommonDataKinds.Phone.NUMBER
                    ))
                ))
        }
        return contacts
    }
    fun getContactByPhoneNumber(number: String) = phoneBookContacts.first { it.number == number }
}