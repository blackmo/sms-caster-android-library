package com.vhl.smscaster.di.module

import com.vhl.smscaster.contacts.PhoneBook
import com.vhl.smscaster.messaging.readers.MessageReader
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

val messingReaderModule = Kodein.Module("messaging") {
   bind() from singleton { MessageReader(instance()) }
}

val modulePhoneBook = Kodein.Module("phonebook") {
   bind() from singleton { PhoneBook() }
}